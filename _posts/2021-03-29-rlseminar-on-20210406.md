---
title:  MLC Seminar on Reinforcement Learning
excerpt: Get your Bearings on RL on April 6, 2021
header:
    overlay_image: /assets/images/2021-01-12-dictionary.jpg
    caption: "Photo credit: [**pexels**](https://pexels.com)"
categories:
  - Seminars
---

It is our pleasure to announce the speaker of our second seminar in 2021: **Fabian Hart (TU Dresden)**. Fabian agreed to provide an   
**Introduction to Reinforcement Learning**   
to our community on **April 6 at 10am** as an online seminar. The abstract for the talk is: 

> The scope of this talk is to introduce Reinforcement Learning (RL) with the interesting practical use case. We discuss the motivation for RL and the basic components and principles. Q learning as a fundamental algorithm for RL is introduced and two variants, DQN and DDPG, are briefly explained. Both variants are presented in the context of the current research project, the development of a two-dimensional ship traffic simulator on the Lower Rhine. 

## Seminar Infrastructure

We facilitate this meeting under the [Dresden Code of Conduct](https://dresden-code-of-conduct.org/de/). **Please be mindful of your peers and supportive in your communication!**

To make the seminar more interactive for everyone, we set up interactives notes [here](https://hackmd.io/@Z3k-IRVbRJuDU-0M-Yfqzw/HJwfyukSu/edit). Please use them to connect to others, submit questions, add further material for the talk. The talk notes will appear on this post after the talk.

## Connection Details

> Time: Apr 6, 2021 10:00 AM Amsterdam, Berlin, Rome, Stockholm, Vienna
> 
> Join Zoom Meeting [through this direct link](https://us02web.zoom.us/j/86727838757?pwd=YXM5eXBiaTFuM0t6V1Jac1JsekxRZz09)
> 
> Meeting ID: `867 2783 8757`
> Passcode: `195131`
>
> One tap mobile
>
> +496971049922,,86727838757#,,,,*195131# Germany
>
> +493056795800,,86727838757#,,,,*195131# Germany
> 
> Dial by your location
>         `+49 69 7104 9922` Germany  
>         `+49 30 5679 5800` Germany  
>         `+49 69 3807 9883` Germany  
>         `+49 695 050 2596` Germany  
>
> Meeting ID: `867 2783 8757`
> Passcode: `195131`
> Find your local number: https://us02web.zoom.us/u/k3CUHOGbn

