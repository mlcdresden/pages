---
title:  After the 2nd MLC Workshop
header:

    overlay_image: /assets/images/2019_06_18_workshopAuditorium.jpg
---

The second workshop of the Machine Learning Community Dresden (MLC) concluded
last month with large success, sparking many discussions among the about 70
participants.  The 20 talks given by presenters from all over Saxony ranged from
the general state of machine learning activities around Dresden to specific
research works and questions.
Most of the slides presented during the workshop are available for download,
links have been added to the
[schedule](../../../2019/05/06/workshopSchedule.html).

The MLC event will a small meeting at TU-Dresden on July 2nd, where Maximilian
Sölch ([VW Data:Lab/TU Munich](https://argmax.ai)) will give a talk about
probabilistic neural models for model-driven control and data-driven
reinforcement learning.
This event will be co-organized by [CEE AI](https://cee-ai.org/).
