---
title: MLC XMAS Event - MLC meets ScaDS.AI Living Lab
excerpt: XMAS networking event
header:
    overlay_image: "https://images.unsplash.com/photo-1612396504363-5f384c8e65a7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1631&q=80"
    caption: "Photo credit: [Alin Andersen](https://unsplash.com/@onixion) on [Unsplash](https://unsplash.com/photos/bVjO0E7Po1Q)"
categories:
  - Events
timezone: Europe/Berlin
date: 2022-12-01 10:00 +0200
---

It is our pleasure to announce the MLC XMAS event in 2022 in cooperation with the ScaDS.AI living lab:  

> **XMAS Event -- MLC meets ScaDS.AI Living Lab** 

presented by MLC and ScaDS.AI to our community on **December 15, 2022, at 04pm - 06pm CEST** as an onsite event.  

## Abstract of the event

There will be some machine learning related short pitches, fun (digital)
games, and plenty of Christmas mood.

To better arrange the event, please mark your participation in the poll:
[https://terminplaner.dfn.de/Kub2vllz2gdg2vre](https://terminplaner.dfn.de/Kub2vllz2gdg2vre)

If you would also like to give a pitch, please send an email with the title to
[mlc-orga@groups.tu-dresden.de](mailto:mlc-orga@groups.tu-dresden.de).
Your pitch should take a maximum of 5 min.

Hope to see you there!

## Location Details

Room 1020, Andreas-Pfitzmann-Bau, Nöthnitzer Str. 46 
Find more details here: [https://navigator.zih.tu-dresden.de/etplan/apb/01/raum/542101.3250](https://navigator.zih.tu-dresden.de/etplan/apb/01/raum/542101.3250).

