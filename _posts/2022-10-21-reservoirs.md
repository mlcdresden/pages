---
title: MLC Seminar on PyRCN
excerpt: Reservoir Computing Networks in Python
header:
    overlay_image: "https://images.unsplash.com/photo-1605537075888-88a61728d8aa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cmVzZXJ2b2lyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600&q=60"
    caption: "Photo credit: [Jonathan Bean](https://unsplash.com/@jonathanbean) on [Unsplash](https://unsplash.com/photos/tb1JFTlse20)"
categories:
  - Seminars
timezone: Europe/Berlin
date: 2022-09-11 12:00 +0200
---

It is our pleasure to announce another seminar in 2022:  

> **PyRCN -- Reservoir Computing Networks in Python** 

presented by Peter Steiner (TU Dresden) to our community on **October 21, 2022, at 10am CEST** as an online seminar.  

## Abstract of the talk

Reservoir Computing Networks (RCNs) belong to a group of machine learning techniques that are simple and yet effective in solving non-linear problems, such as classification or regression. With a significantly easier training process than state-of-the-art deep neural networks, such as Convolutional Neural Networks (CNNs) and LSTMs, they achieve comparable results in various classification and regression tasks. In this talk, we introduce RCNs together with our publicly available Python toolbox PyRCN (Python Reservoir Computing Networks) for optimizing, training and analyzing RCNs on arbitrarily large datasets. It provides a platform for educational and exploratory analyses of RCNs, as well as a framework to apply RCNs on complex tasks including sequence processing.

## Seminar Details

We facilitate this meeting under the [Dresden Code of Conduct](https://dresden-code-of-conduct.org/de/). **Please be mindful of your peers and supportive in your communication!**

To make the seminar more interactive for everyone, we set up interactives [notes](https://notes.desy.de/7Y5W9_oLSNGiPWBRHbDqFg). Please use them to connect to others, submit questions, add further material for the talk. The talk notes will appear on this website after the talk.

## Connection Details

Are shared in the [notes](https://notes.desy.de/7Y5W9_oLSNGiPWBRHbDqFg).
