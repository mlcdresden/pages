---
title:  MLC Seminar on Interpretable Classifications
excerpt: Machine Learning Seminar on March 15, 2022
header:
    overlay_image: /assets/images/thinker-neonbrand-2RRq1BHPq4E-unsplash_1024px.jpg
    caption: "Photo credit: [**unsplash.com**](https://unsplash.com/photos/2RRq1BHPq4E)"
categories:
  - Seminars
timezone: Europe/Berlin
date: 2022-02-21 08:00 +0200
---

It is our pleasure to announce the speaker of our first seminar in 2022: **Thomas Villmann (HS Mittweida)**. 

Thomas agreed to provide a discussion about

**Interpretable Models for Classification Learning - Performance is not enough**

to our community on **March 15, 2022, at 10am** as an online seminar (see connection details below). 

## Abstract of the talk

> Classification learning currently is dominated by deep neural networks, which frequently achieve best performance for a given task. Moreover, due to sophisticated end-to-end learning methods and strategies as well as pre-trained modules for many application tasks, these networks became popular for a broad community in applied AI development. The latter one also reduce the problems of the requirement of huge data bases for adequate training as well as the time consuming training due to vanishing gradients.
> 
> Another major drawback of these deep networks is that usually they act as black-box approaches. Many attempts are made to explain their behavior including saliency/heat maps, simplified models imitating the behavior and visual explanations.
> 
> Following C. Rudin, a better approach is to require interpretability for a machine learning model by design. One of the most prominent interpretable approaches in unsupervised and supervised learning is based on vector quantization.
>
> In the talk, we will show recent developments for classification learning based on vector quantization models. We give the basic mathematical justifications for several related approaches together with their possible application areas from a methodology point of view. Further, we demonstrate the interpretation possibilities of those networks, which follow from the model design.

## About our speaker

**Thomas Villmann** is the director of the Saxon Institute for Computational Intelligence and Machine Learning (SICIM) at the University of Applied Sciences Mittweida. If you would like to explore more about Thomas, allow us to refer to his professional [website](https://www.cb.hs-mittweida.de/webs/villmann.html). 

## Seminar Infrastructure

We facilitate this meeting under the [Dresden Code of Conduct](https://dresden-code-of-conduct.org/de/). **Please be mindful of your peers and supportive in your communication!**

To make the seminar more interactive for everyone, we set up interactives notes [here](https://notes.desy.de/t3nZypI2Sf2Z75JhmH8xug?edit). Please use them to connect to others, submit questions, add further material for the talk. The talk notes will appear on this website after the talk.

## Connection Details

> Join Zoom Meeting
> https://us06web.zoom.us/j/95240535967?pwd=VkY2UlFscjkwM2xidC82QlpYbWY5UT09
>
> Meeting ID: 952 4053 5967
> Passcode: 690757
> One tap mobile
> +496971049922,,95240535967#,,,,*690757# Germany
> +496938079883,,95240535967#,,,,*690757# Germany
>
> Dial by your location
>         +49 69 7104 9922 Germany
>         +49 69 3807 9883 Germany
>         +49 69 3807 9884 Germany
>         +49 69 5050 0951 Germany
>         +49 69 5050 0952 Germany
>         +49 695 050 2596 Germany
>         +1 720 707 2699 US (Denver)
>         +1 253 215 8782 US (Tacoma)
>         +1 301 715 8592 US (Washington DC)
>         +1 312 626 6799 US (Chicago)
>         +1 346 248 7799 US (Houston)
>         +1 646 558 8656 US (New York)
> Meeting ID: 952 4053 5967
> Passcode: 690757
> Find your local number: https://us06web.zoom.us/u/kdCCl6grIR

