---
title: MLC Seminar on Prediction of designer-recombinases for DNA editing with generative deep learning
excerpt: Unsupervised Generation of Genome Sequences
header:
    overlay_image: "https://images.unsplash.com/photo-1633167606207-d840b5070fc2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1052&q=80"
    caption: "Photo credit: [anirudh](https://unsplash.com/@lanirudhreddy) on [Unsplash](https://unsplash.com/photos/YQYacLW8o2U)"
categories:
  - Seminars
timezone: Europe/Berlin
date: 2022-08-01 12:00 +0200
---

It is our pleasure to announce another seminar in 2022:  

> **Prediction of designer-recombinases for DNA editing with generative deep learning** 

presented by Lukas T. Schmitt (TU Dresden) to our community on **September 12, 2022, at 11am** as an online seminar. 

## Abstract of the talk

Site-specific tyrosine-type recombinases are effective tools for genome engineering, with the first engineered variants having demonstrated therapeutic potential. So far, adaptation to new DNA target site selectivity of designer-recombinases has been achieved mostly through iterative cycles of directed molecular evolution. While effective, directed molecular evolution methods are laborious and time consuming. Here we present RecGen (Recombinase Generator), an algorithm for the intelligent generation of designer-recombinases. We gathered the sequence information of over two million Cre-like recombinase sequences evolved for 89 different target sites with which we trained Conditional Variational Autoencoders for recombinase generation. Experimental validation demonstrated that the algorithm can predict recombinase sequences with activity on novel target-sites, indicating that RecGen is useful to accelerate the development of future designer-recombinases.

## Seminar Infrastructure

We facilitate this meeting under the [Dresden Code of Conduct](https://dresden-code-of-conduct.org/de/). **Please be mindful of your peers and supportive in your communication!**

To make the seminar more interactive for everyone, we set up interactives [notes](https://notes.desy.de/7Y5W9_oLSNGiPWBRHbDqFg). Please use them to connect to others, submit questions, add further material for the talk. The talk notes will appear on this website after the talk.

## Connection Details

Are shared in the notes.
