---
title: MLC Seminar introducing kernel methods and Gaussian processes
excerpt: Introducing Kernel Methods and Gaussian Processes
header:
    overlay_image: "https://images.unsplash.com/photo-1598639298075-9f62f1fc5463?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1600&q=80"
    caption: "Photo credit: [Nathalia Segato](https://unsplash.com/@trintaycinco) on [Unsplash](https://unsplash.com/photos/HOYNVDyDMbA)"
categories:
  - Seminars
timezone: Europe/Berlin
date: 2023-01-20 08:00 +0200
---

It is our pleasure to announce the first seminar in 2023:  

> **Introduction to kernel methods and Gaussian processes** 

presented by Steve Schmerler (HZDR) to our community on **February 06, 2023, at 1:30pm CET** as an online seminar.  

## Abstract of the talk

Kernel ridge regression (KRR) is an example of a kernel method. It represents the family of “classical” machine learning techniques (e.g. no neural networks) that is popular in application areas where data is not necessarily abundant and where one can build expressive models by using (physics-informed) engineered features. A closely related method is Gaussian process regression (GPR), whose foundations lie in Bayesian statistics and which can provide powerful uncertainty information. In this introduction, Steve will cover the mathematical foundations of those methods, shed light onto their close relation and discuss when they may be a useful parallel approach to more flexible models such as neural networks.

## Seminar Details

We facilitate this meeting under the [Dresden Code of Conduct](https://dresden-code-of-conduct.org/de/). **Please be mindful of your peers and supportive in your communication!**

To make the seminar more interactive for everyone, we set up interactives [notes](https://notes.desy.de/-Y8TPFrDREa6u5J8_radPA?edit). Please use them to connect to others, submit questions, add further material for the talk. The talk notes will appear on this website after the talk.

## Connection Details

Are shared in the [notes](https://notes.desy.de/-Y8TPFrDREa6u5J8_radPA?edit).
