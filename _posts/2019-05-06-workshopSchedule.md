---
title:  Schedule for the 2nd MLC Workshop
header:

    overlay_image: /assets/images/2019_05_16_workshopSchedule.jpg
---

# 2nd MLC Workshop Schedule

Registrations for the [second workshop](../../../2019/03/01/workshop.html) of the
Machine Learning Community Dresden (MLC) on the May 16, 2019 are closed now. More than 60
registered participants will be attending the 21 scheduled talks. The final
schedule is as follows:

|Time|Presenter|Affiliation|Title|
|---|---|---|---|
|10:00 – 11:45|MLC-Orga-Team||Welcome Address|
| |Graham Appleby |HZDR|Center of Advanced Systems Understanding (CASUS)|
| |Guido Juckeland|HZDR|[Helmholtz Artificial Intelligence Cooperation Unit (HAICU)](https://cloudstore.zih.tu-dresden.de/index.php/s/UbSVZG48EsaMYRy)|
| |Nico Hoffmann|HZDR|[Learning partial differential equations via neural networks](https://cloudstore.zih.tu-dresden.de/index.php/s/XaMt5sSpGMEw5cZ)|
| |Stefan Ecklebe|TU Dresden|[Model identification for process control applications using machine learning](https://cloudstore.zih.tu-dresden.de/index.php/s/gfBAHQcuhdrt1eO)|
| |Lena Jurkschat|ScaDS|NER on financial documents|
| |Andreas Gocht|TU Dresden|[A New Approach for Automated Feature Selection](https://cloudstore.zih.tu-dresden.de/index.php/s/XSimZXZWNUp4aXf)|
|11:45 – 12:30  | | |Break|
|12:30 – 14:30|Norman Koch|ScaDS|[Automatic highly-parallelized hyperparameter-optimization for Machine Learning Algorithms](https://cloudstore.zih.tu-dresden.de/index.php/s/EnwznokGLiJ12DD)|
| |Andreas Knüpfer|TU Dresden|[The HP-DLF Scalable Node-Parallel Deep Learning Framework](https://cloudstore.zih.tu-dresden.de/index.php/s/B1ZNH6stwtJlw39)|
| |Stefan Reitmann|DLR|[Usage of machine learning in modern data-based air traffic management](https://cloudstore.zih.tu-dresden.de/index.php/s/2fOLkbIAMq7MjrA)|
| |Bernhard Vogginger|TU Dresden|[SpiNNaker2 - a Scalable Hardware Architecture for Real-time Neural Networks made in Dresden](https://cloudstore.zih.tu-dresden.de/index.php/s/cfbnR6i3TyPoOMy)|
| |Simon Walz|TU Dresden|Show me that 3D model of an LSTM Autoencoder with many units on multivariate data|
| |Danell Quesada|TU Dresden|[Statistical Downscaling of CMIP5 projections for Costa Rica employing Artificial Neural Networks](https://cloudstore.zih.tu-dresden.de/index.php/s/sy8PMyh87hxCUSl)|
| |Lennart Schmidt|UFZ Leipzig|Automated Quality-Control of Environmental Sensor Data|
|14:30 – 15:00| | |Break|
|15:00 – 17:15|Frank Fitzek|TU Dresden|Tactile Internet|
| |Sebastian Hahn|TU Dresden|Deep Structured Models for Semantic Segmentation of OCT-Scans of Oral Tissue|
| |Sarah Schmell|Biotec|Automation of image-based routine diagnostics via deep learning approaches in pathology|
| |Felix Knorr|TU Dresden|[How well can an SVM deal with noise and small samples](https://cloudstore.zih.tu-dresden.de/index.php/s/CLFGZQ7wcJPy8z4)|
| |Peter Steinbach|MPI-CBG|[Adversarial Attacks on Medical Imaging Revisited](https://github.com/psteinb/mlc-reproducing-adversarial-medicine)|
| |Ronald Tetzlaff|TU Dresden|Bio-inspired computing by Cellular Neuronal Networks|
| |Pavol Mikolas|TU Dresden|[Development of a machine learning classifier to identify ADHD in real-world clinical data](https://cloudstore.zih.tu-dresden.de/index.php/s/jr4GyM5LKbhfOeV)|
| |Sebastian Starke|HZDR/OncoRay|[Deep-learning based prediction of cancer recurrence risks](https://cloudstore.zih.tu-dresden.de/index.php/s/D4vYxlO5Wc9VlXq)|
|17:15 – 17:30| | |Wrap-up|
