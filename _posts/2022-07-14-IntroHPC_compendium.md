---
title: MLC Seminar on new ZIH HPC compendium
excerpt: Seminar ZIH HPC Compendium July 14, 2022
header:
    overlay_image: /assets/images/diego-ph-fIq0tET6llw-unsplash.jpg
    caption: "Photo credit: [**unsplash.com**](https://unsplash.com/photos/fIq0tET6llw)"
categories:
  - Seminars
timezone: Europe/Berlin
date: 2022-05-30 12:00 +0200
---

It is our pleasure to announce a further seminar in 2022:
**Introduction to new ZIH HPC compendium** presented by Martin Schroschk and Christoph Lehmann (ZIH TU Dresden) to our community on **July 14, 2022, at 10am** as an online seminar. 
The Center for Information Services and High Performance Computing (ZIH) at TU Dresden provides HPC resources for researchers. These resources are also available to researchers who are not members of TU Dresden.

## Abstract of the talk

This talk will provide an overview of the new ZIH HPC compendium, that is typically a first point of reference for users of the ZIH HPC system. 
Thereby, the new HPC compendium is also of interest for users of other HPC systems as it contains articles of a generic use.
An important feature of the new HPC compendium is the possibility of active user contibution by directly opening issues and even writing articles. 
For the talk a main focus will lie on aspects/questions of machine learning and data analytics usage of the HPC system. 
Please note, that access to the ZIH HPC system is possible as well for people who are not members of the TU Dresden. 

## Seminar Infrastructure

We facilitate this meeting under the [Dresden Code of Conduct](https://dresden-code-of-conduct.org/de/). **Please be mindful of your peers and supportive in your communication!**

To make the seminar more interactive for everyone, we set up interactives notes [here](https://hackmd.io/to/be/added/). Please use them to connect to others, submit questions, add further material for the talk. The talk notes will appear on this website after the talk.

## Connection Details

This is an online meeting via BigBlueButton.

Participants with ZIH login:
[https://selfservice.zih.tu-dresden.de/l/link.php?m=185544&p=1e5a8705](https://selfservice.zih.tu-dresden.de/l/link.php?m=185544&p=1e5a8705)

Participants without ZIH login:
[https://selfservice.zih.tu-dresden.de/link.php?m=185544&p=8ef6332a](https://selfservice.zih.tu-dresden.de/link.php?m=185544&p=8ef6332a)

