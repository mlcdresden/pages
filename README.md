[![pipeline status](https://gitlab.com/mlcdresden/pages/badges/master/pipeline.svg)](https://gitlab.com/mlcdresden/pages/commits/master)

---

# MLC Dresden Website

This is the repository to render the website of the MLC Dresden.

## How to render the website locally

This website is rendered using jekyll. It uses the [minimalistik mistakes theme](https://github.com/mmistakes/minimal-mistakes). This theme requires some additional software to be installed. The following instructions assume that you are running macOS or a flavor of Linux and that you have the following installed:

- ruby including ruby development packages (currently, only ruby versions lower than `3.0` are working)
- gem, the ruby package manager

_Hint_: There are issues installing older ruby versions (`<3.0.x`) on Ubuntu 22.04 and Fedora 36. A solution can be found at [https://github.com/rvm/rvm/issues/5209#issuecomment-1326593911](https://github.com/rvm/rvm/issues/5209#issuecomment-1326593911).

To render the website locally, do:

``` bash
$ git clone git@gitlab.com:mlcdresden/pages.git
$ cd pages
$ make prepare
#a lot of output
$ make serve
bundle exec jekyll serve
Configuration file: /some/path/pages/_config.yml
            Source: /some/path/pages
       Destination: /some/path/pages/_site
 Incremental build: disabled. Enable with --incremental
      Generating... 
                    done in 12.315 seconds.
 Auto-regeneration: enabled for '/some/path/pages'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
```

Then open a browser and go to the URL: `http://127.0.0.1:4000`

## How to add posts

The entire website is driven by markdown files. Feel free to study the [documentation](https://kramdown.gettalong.org/quickref.html) about authoring pages with markdown flavor used here.

So in order to contribute a post, create a branch first:

``` bash
$ git checkout -b my-new-post
$ cd _posts
$ ${EDITOROFCHOICE} year-month-day-title.md 
```

Note that the date in the filename  is important. It will be the date on which the post will be published. The markdown file has to have the following front-matter before any real markdown can be put:

``` markdown
---
title:  Minutes of the Organisation Team
---
```

The `layout` key is the most important. It defines the layout of the html page later-on. The `title` key defines what is printed as the blog title. The `featured-img` is optional, it's value defines the file base name of a corresponding image file which will be visible as the post image.

### Images in Posts

If you wish to add images in posts, then put them into `assets`. From within the markdown of a blog post, you can then include the image by doing:

``` markdown
![Catchment Map]({{site.baseurl}}/assets/2019-10-02-map.png)
(source[^source2])
```

